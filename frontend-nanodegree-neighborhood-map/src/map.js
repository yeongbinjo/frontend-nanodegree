'use strict';

// Initialize Google Map
function initMap() {
  const korea = {lat: 36.629, lng: 127.929};
  const map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: korea
  });
  initializeApp(map);
}

// Open InfoWindow
const openWindow = function(map, markerWrapper) {
  if (markerWrapper.infoWindow) {
    markerWrapper.infoWindow.open(map, markerWrapper.marker);
  } else {
    const infoWindow = new google.maps.InfoWindow({
      content: '<img src="spinner.gif" width="45px" height="45px">'
    });
    markerWrapper.infoWindow = infoWindow;
    infoWindow.open(map, markerWrapper.marker);

    // Get data from Wikipedia and update InfoWindow
    $.ajax({
      url: 'https://en.wikipedia.org/w/api.php?action=query' +
      '&prop=revisions&rvprop=content&rvsection=0&format=json&origin=*&titles=' + markerWrapper.location.name,
      type: 'GET',
      crossDomain: true
    }).done(function(data) {
      infoWindow.setContent('<p>' + data.query.pages[Object.keys(data.query.pages)[0]].title + '</p><p><em>from Wikipedia</em></p>');
    }).fail(function() {
      infoWindow.close();
      showToast('Some problem occurred when retrieve data from Wikipedia. Please try again later.');
    });
  }
};

// Show toast
const showToast = function(message) {
  document.querySelector('#demo-toast-example').MaterialSnackbar.showSnackbar({message: message});
};

// Intialize Marker
const createMarker = function(map, location) {
  // Create marker
  return new google.maps.Marker({
    position: location.position,
    map: map,
    animation: google.maps.Animation.DROP
  });
};

// Attach event listener to marker
const attachListener = function(map, markerWrapper) {
  // Open InfoWindow when click marker on map
  markerWrapper.marker.addListener('click', function() {
    openWindow(map, markerWrapper);
    toggleBounce(markerWrapper.marker);
  });
};

// Toggle bounce animation of marker
function toggleBounce(marker) {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}

const googleError = function() {
  alert('Can\'t load Google Map API appropriately. Please try again later.')
};
