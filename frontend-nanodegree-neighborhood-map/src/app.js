'use strict';

const Location = function(name, kr_name, position, marker) {
  this.name = name;
  this.kr_name = kr_name;
  this.position = position;

  this.displayName = ko.computed(function () {
    return this.name + ' (' + this.kr_name + ')';
  }, this);
};

const MarkerWrapper = function(marker, location) {
  this.marker = marker;
  this.infoWindow = null;
  this.location = location;
};

const LocationViewModel = function(map) {
  // Predefined locations
  this.predefinedLocations = [
    new Location('Seoul', '서울', {lat: 37.533, lng: 127.025}),
    new Location('Incheon', '인천', {lat: 37.469, lng: 126.573}),
    new Location('Gwangju', '광주', {lat: 35.126, lng: 126.831}),
    new Location('Daegu', '대구', {lat: 35.798, lng: 128.583}),
    new Location('Ulsan', '울산', {lat: 35.519, lng: 129.239}),
    new Location('Daejeon', '대전', {lat: 36.322, lng: 127.379}),
    new Location('Busan', '부산', {lat: 35.198, lng: 129.054}),
    new Location('Gyeonggi Province', '경기도', {lat: 37.567, lng: 127.190}),
    new Location('Gangwon Province', '강원도', {lat: 37.556, lng: 128.209}),
    new Location('North Chungcheong Province', '충청북도', {lat: 36.629, lng: 127.929}),
    new Location('South Chungcheong Province', '충청남도', {lat: 36.557, lng: 126.780}),
    new Location('North Gyeongsang Province', '경상북도', {lat: 36.249, lng: 128.665}),
    new Location('South Gyeongsang Province', '경상남도', {lat: 35.260, lng: 128.665}),
    new Location('North Jeolla Province', '전라북도', {lat: 35.717, lng: 127.144}),
    new Location('South Jeolla Province', '전라남도', {lat: 34.819, lng: 126.893}),
    new Location('Jeju Island', '제주도', {lat: 33.365, lng: 126.543})
  ];

  // You can filter by both English and Korean
  this.filterText = ko.observable("");

  this.clearFilter = (function() {
    this.filterText("");
  }).bind(this);

  this.handleClick = (function(markerWrapper) {
    // update filterText
    this.filterText(markerWrapper.location.name);

    // Open InfoWindow when click item of list
    openWindow(map, markerWrapper);
  }).bind(this);

  // Initialize marker and marker wrappers from predefined locations
  const predefinedMarkerWrappers = [];
  this.predefinedLocations.forEach(function(location) {
    let marker = createMarker(map, location);
    const markerWrapper = new MarkerWrapper(marker, location);
    attachListener(map, markerWrapper);
    predefinedMarkerWrappers.push(markerWrapper);
  });

  // markers are which show in map
  this.markers = ko.computed(function() {
    if (!this.filterText()) {
      // Show all of markers if filterText is not defined
      predefinedMarkerWrappers.forEach(function(markerWrapper) {
        markerWrapper.marker.setMap(map);
      });
      return predefinedMarkerWrappers;
    } else {
      // Show filtered markers
      const filteredMarkers = [], filterText = this.filterText().toLowerCase();
      predefinedMarkerWrappers.forEach(function (markerWrapper) {
        if (markerWrapper.location.name.toLowerCase().includes(filterText) || markerWrapper.location.kr_name.includes(filterText)) {
          filteredMarkers.push(markerWrapper);
          markerWrapper.marker.setMap(map);
          markerWrapper.marker.setAnimation(google.maps.Animation.BOUNCE);
        } else {
          markerWrapper.marker.setMap(null);
        }
      });
      return filteredMarkers;
    }
  }, this);
};

const initializeApp = function(map) {
  // initilizeApp called in `initMap`
  ko.applyBindings(new LocationViewModel(map));
};
